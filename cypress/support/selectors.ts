export const getHook = name => cy.get(`[hook="${name}"]`)

export const getHookStartsWith = name => cy.get(`[hook^="${name}"]`)

export const getHookInput = name => cy.get(`input[hook="${name}"]`)

export const getHookButton = name =>
    cy.get(`button[hook="${name}"]`)

export const getByClassName = className => {
    return cy.get(`[class*=${className}]`)
}
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import { getHookInput, getHookButton, getHook } from './selectors'

Cypress.Commands.add('searchBy', (searchOption, name) => {
    getHookInput(`search-${searchOption}`).click()
    getHookInput('search-input').clear().type(name)
    getHookButton('search-button').click()
    getHook('loading').should('not.exist')
})

Cypress.Commands.add('verifyPerson', (personData) => {
    const { name, gender, birthYear, eyeColor, skinColor } = personData
    getHook('character-name').contains(name)
    getHook('gender').should('have.text', gender)
    getHook('birth-year').should('have.text', birthYear)
    getHook('eye-color').should('have.text', eyeColor)
    getHook('skin-color').should('have.text', skinColor)
})

Cypress.Commands.add('verifyPlanet', (planetData) => {
    const { name, population, climate, gravity } = planetData
    getHook('planet-name').contains(name)
    getHook('population').should('have.text', population)
    getHook('climate').should('have.text', climate)
    getHook('gravity').should('have.text', gravity)
})
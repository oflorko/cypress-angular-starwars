/// <reference types="cypress" />
const testDataPeople = require('../fixtures/testData/people.json');
const testDataPlanets = require('../fixtures/testData/planets.json')

describe("Search by people returns expected result", () => {
  beforeEach(() => {
    cy.visit('/')
  });

  testDataPeople.forEach(testCase => {
    it(`Search people by ${testCase.description}`, () => {
      cy.searchBy('people', testCase.name)
      cy.verifyPerson(testCase)
    });
  });
});

  describe("Search by planets returns expected result", () => {
    beforeEach(() => {
      cy.visit('/')
    });
  
    testDataPlanets.forEach(testCase => {
      it(`Search planets by ${testCase.description}`, () => {
        cy.searchBy('planets', testCase.name)
        cy.verifyPlanet(testCase)
      });
    });
});

 # Assessment solution

IMPORTANT: Please provide me emails of the reviewers, so I can grant the access to the project and Cypress dashboard.

# Introduction
I would like to confess that it was the first time I was using angular in my project. Also I tried to lime myself in recommended 4 hours of work, so some things in my solution definitely deserve improvement.

During tests development node version `12.20` was used, works fine.
Version of my local angular was `7.3.10`

## JS or TS?
I see that in the project itself some files have extensions `.js` and some `.ts`. TypeScript support is enabled, however I used JavaScript everywhere I could to save time.

# Tests implementation

## Selectors
I started with UI inspection, there are not that many elements to interact, to make tests and all complimentary methods more readable I decided to add hooks to the elements of UI. 

Next I added `cypress/support/selectors.ts` to make getters look shorter and to refer to elements only with hook names.

## Test data
First of all I checked `swapi.dev` to see what kind of data we operating with and picked some unusual cases.
I knew that I am going to run search and verify returned data for several times. So, I was looking how to do that in cycle, using the same test.
My inspiration came from this [article](https://kevintuck.co.uk/write-once-and-run-multiple-test-cases-with-cypress/) to use JavaScript's built in looping.

In cucumber I would use Examples for the same purpose.

## Custom commands
There are three custom commands added to perform search and data verification for people and planets.

## To be tested
Some tests cases I deliberately left not covered because of time limitations for this assignment. Among them:
- test multiple search results
- test search by low case input
- negative testing

## Mocha and Cucumber
It was not clear to me from the assignment description if I should write tests using both frameworks or chose one of them. I decided to enable cucumber support anyway. What I noticed that it disables tests created earlier using mocha. So cucumber support exists in separate branch [cucumber](https://gitlab.com/oflorko/cypress-angular-starwars/-/commit/32921ede17b02381d72a7ffbf28df8a01325e45a)

## Issues
There are spaces before and after card values that may cause problems with data processing

# Reporting
As a reporting tool I chose [Cypress dashboard](https://dashboard.cypress.io/projects/osuiuv/runs/2/overview) tests are being recorded there.
![](./cypress/report/screenshots/dashboard.png)

# Continuous integration in GitLab
In tests assignment it was recommended to use GihHub, I've been working with GitLab during the last three years and wanted to prepare ready to use CI for this assignment. Please refer to `.gitlab-ci.yml`.
Code is being compiled in docker container, unfortunately I have not found reliable image with required combination of Node/Angular/Cypress/Chrome version and it was far beyond the scope of this assignment to create my own image. So the pipelines are failing right now.

# Run Cypress tests with Angular builder
With my very last commit to master branch I am fixing `angular.json` to run cypress tests from the Angular CLI using [ngx-cypress-builder](https://github.com/isaacplmann/ngx-cypress-builder)
Tests are running now with `ng e2e` command
